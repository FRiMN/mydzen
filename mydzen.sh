#!/bin/sh

#***********************************************************************#
#                                                                       #
#                              mydzen.sh                                #
#                  by Nik Volkov (freezemandix@ya.ru)                   #
#                            Сентябрь 2014                              #
#                                                                       #
# Скрипт для панели dzen2. Разделён на блоки: констант, устанавливаемых #
# при запуске скрипта (CONSTANTS); функций, формирующих строки для      #
# панели или выполняющих другие действия (FUNCTIONS); и главного цикла  #
# скрипта, формирующего итоговую строку для dzen2 (MAIN LOOP).          #
#                                                                       #
# P.S.: Я считаю, что у меня не плохая фантазия, но как показывает      #
# история, многие хорошие вещи имеют глупые названия :)                 #
#                                                                       #
# Demand:                                                               #
#   dzen2 >= 0.8.5-4                                                    #
#   mocp                                                                #
#   amixer                                                              #
#   remind                                                              #
#   xprop                                                               #
#                                                                       #
# Info:                                                                 #
#   /usr/share/doc/dzen2/                                               #
#                                                                       #
#***********************************************************************#





#----------------------------------
#
#   CONSTANTS
#
#----------------------------------


# доставить кириллистических xfonts и `xset +fp /usr/share/fonts/X11/cyrillic`
# `xfontsel`
#FONT="-*-courier-medium-r-*-*-14-*-*-*-*-*-*-*"
#FONT="-*-helvetica-medium-r-*-*-14-*-*-*-*-*-*-*"
#FONT="-*-clean-medium-r-*-*-13-*-*-*-*-*-koi8-*"
#FONT="-*-fixed-*-*-*-*-13-*-*-*-*-*-koi8-*"
FONT="-*-terminus-bold-r-*-*-12-*-*-*-*-*-koi8-*"

COLOR_DARK="#666666"
COLOR_LIGHT=""
COLOR_ACTIVE="#A5E12D"
COLOR_BACK="#333333"
#COLOR_BACK="black"

COLOR_FLAG=1

FULLSCR_FLAG=0
FIRST_TURN=1    # первый проход скрипта (нужно, т.к. dzen2 запускается в конце скрипта, до этого момента pid dzen'а неопределён)

REMIND_COUNTER=0
REMIND_BOOSTER=18   # чем больше число, тем дольше показываются напоминания; должно быть больше 0
REMIND_TMP="/tmp/remind.list"

USER=`whoami`

# для lightsOn
mplayer_detection=1
vlc_detection=1
firefox_flash_detection=1
chromium_flash_detection=1
html5_detection=1





#----------------------------------
#
#   FUNCTIONS
#
#----------------------------------


# данные из mocp
MocpData (){
    MOCP_SONG=`mocp --format "%title" | sed -e "s/ \* / /g" | grep -Eo '^.{,140}'`  # проблема со звёздочкой
    MOCP_STIME_FULL=`mocp --format "%ts"`
    MOCP_STIME_CRNT=`mocp --format "%cs"`
    MOCP_TIME_FULL=`mocp --format "%tt"`
    MOCP_TIME_CRNT=`mocp --format "%ct"`

    if [ -z "$MOCP_SONG" ]  # если нет заголовка, то берем имя файла
    then
        MOCP_SONG=`mocp --format "%file" | sed -e "s/ \* / /g" | grep -Eo '^.{,140}'`
    fi

    if [ -z "$MOCP_STIME_FULL" ]    # пустая строка (сетевое радио)
    then
        MOCP_TIME_PROC="--"
        MOCP_TIME_FULL="NET"
    else
        MOCP_TIME_PROC=`expr 100 \* $MOCP_STIME_CRNT / $MOCP_STIME_FULL`
    fi

    if (( ${#MOCP_TIME_PROC} < 2 ))
    then
        MOCP_TIME_PROC=" $MOCP_TIME_PROC"
    fi
}

MocpInfo (){
    if [ -e "/home/$USER/.moc/pid" ]    # запущен ли сервер mocp
    then
        MOCP_STATE=`mocp --info | grep "State" | awk '{print $2}'`

        case $MOCP_STATE in
            "STOP")
                MOCP_STR="^fg($COLOR_DARK)^i(/usr/share/dzen2/bitmaps/music.xbm)^fg() ^fg($COLOR_DARK)MOCP остановлен^fg()"
                ;;
            "PLAY")
                MocpData
            
                MOCP_STR="^i(/usr/share/dzen2/bitmaps/music.xbm) ^fg($COLOR_DARK)[$MOCP_TIME_CRNT/$MOCP_TIME_FULL]^fg()  ^fg($COLOR_ACTIVE)$MOCP_TIME_PROC%^fg()  $MOCP_SONG"
                ;;
            "PAUSE")
                MocpData
                
                if [ $COLOR_FLAG = 0 ]
                then
                    COLOR=$COLOR_LIGHT
                    COLOR_FLAG=1
                else
                    COLOR=$COLOR_DARK
                    COLOR_FLAG=0
                    MOCP_TIME_PROC="--"
                fi
                MOCP_STR="^fg($COLOR)^i(/usr/share/dzen2/bitmaps/music.xbm)^fg() ^fg($COLOR_DARK)[$MOCP_TIME_CRNT/$MOCP_TIME_FULL]^fg()  ^fg($COLOR_ACTIVE)$MOCP_TIME_PROC%^fg()  $MOCP_SONG"
        esac
    else
        MOCP_STR="^fg($COLOR_DARK)Сервер MOCP не запущен^fg()"
    fi
}

# индикатор звука
VolControl (){
    VOL_TRIG=`amixer sget Master | grep "Left: Playback" | cut -d [ -f 3 | cut -d ] -f 1`   # on/off (mute)
    
    if [ $VOL_TRIG = "on" ]
    then
        VOL=`amixer sget Master | grep "Left: Playback" | cut -d [ -f 2 | cut -d % -f 1`
        VOL="$VOL%"
    else
        VOL="MUTE"
    fi

    VOL_STR="^i(/usr/share/dzen2/bitmaps/volume.xbm) $VOL"
}

BatInfo (){
    BAT_NOW=`cat /sys/class/power_supply/BAT1/charge_now`
    BAT_FULL=`cat /sys/class/power_supply/BAT1/charge_full`
    BAT_PROC=`expr 100 \* $BAT_NOW / $BAT_FULL`
    #BAT_PROC=50

    if (( $BAT_PROC <= 15 ))
    then
        # мигание при низком заряде батареи
        if [ "$BAT_COLOR" = "$COLOR_ACTIVE" ]
        then
            BAT_COLOR=$COLOR_DARK
        else
            BAT_COLOR=$COLOR_ACTIVE
        fi
    else
        if (( $BAT_PROC < 80 ))
        then
            BAT_COLOR=$COLOR_LIGHT
        else
            if (( $BAT_PROC <= 100 ))
            then
                BAT_COLOR=$COLOR_DARK
            fi
        fi
    fi

    BAT_STR="^fg($COLOR_DARK)BAT^fg() ^fg($BAT_COLOR)$BAT_PROC%^fg()"
}

# данные из remind
RemindInfo (){
    if (( $REMIND_BOOSTER <= 0 ))
    then
        echo "Недопустимое значение: REMIND_BOOSTER должен быть больше 0" >&2
        echo "Аварийная остановка!" >&2
        exit 1
    fi
    
    remind -m -b1 -s+1 "/home/$USER/.reminders" > $REMIND_TMP   # список напоминаний
    LINE_END=`cat $REMIND_TMP | wc -l`  # кол-во строк/напоминаний

    if (( REMIND_COUNTER < LINE_END \* REMIND_BOOSTER ))
    then
        let "LINE = $REMIND_COUNTER / $REMIND_BOOSTER + 1"
        REMIND_DATE=`sed -e "$LINE!d" $REMIND_TMP | awk '{print $1}'`
        #REMIND_DAY=`echo $REMIND_DATE | awk -F"/" '{print $3}'`
        #REMIND_MONTH=`echo $REMIND_DATE | awk -F"/" '{print $2}'`
        #REMIND_YEAR=`echo $REMIND_DATE | awk -F"/" '{print $1}'`
        REMIND_BODY=`sed -e "$LINE!d" $REMIND_TMP | cut -d ' ' -f 6- | grep -Eo '^.{,100}'`
        (( REMIND_COUNTER++ ))
    else
        REMIND_COUNTER=0
    fi
    #REM_STR="^fg($COLOR_DARK)[$LINE/$LINE_END]^fg() ^fg($COLOR_DARK)$REMIND_MONTH/^fg()^fg($COLOR_ACTIVE)$REMIND_DAY^fg() $REMIND_BODY"
    REM_STR="^fg($COLOR_DARK)[$LINE/$LINE_END]^fg() ^fg($COLOR_ACTIVE)$REMIND_DATE^fg() $REMIND_BODY"
}

# решение проблемы с fullscreen'ом
LightsOn (){
    # нагло сперто с https://raw.github.com/hotice/lightsOn/master/lightsOn.sh
    checkFullscreen

    # установка PID и посыл сигналов dzen2 на скрытие/показ панели
    if [ -n "$PID" ]
    then
        if [ $FULLSCR_FLAG = 1 ]
        then
            /bin/kill -s USR1 $PID  # signal USR1 (скрыть панель)
        else
            /bin/kill -s USR2 $PID  # signal USR2 (показать панель)
        fi
    else
        if [ $FIRST_TURN = 0 ]
        then
            PID=`ps -ef | grep -E "dzen2[^\.]+sigusr1" | awk '{print $2}'`
        fi
    fi
}

checkFullscreen()
{
    #get id of active window and clean output
    activ_win_id=`xprop -root _NET_ACTIVE_WINDOW`
    #activ_win_id=${activ_win_id#*# } #gives error if xprop returns extra ", 0x0" (happens on some distros)
    activ_win_id=${activ_win_id:40:9}

    # Skip invalid window ids (commented as I could not reproduce a case
    # where invalid id was returned, plus if id invalid
    # isActivWinFullscreen will fail anyway.)
    if [ "$activ_win_id" = "0x0" ]; then
         continue
    fi
    
    # Check if Active Window (the foremost window) is in fullscreen state
    isActivWinFullscreen=`xprop -id $activ_win_id | grep _NET_WM_STATE_FULLSCREEN`
    if [[ "$isActivWinFullscreen" = *NET_WM_STATE_FULLSCREEN* ]]
    then
        isAppRunning
        var=$?
        if [[ $var -eq 1 ]]
        then
            FULLSCR_FLAG=1
        fi
    else
        FULLSCR_FLAG=0
    fi
}

isAppRunning()
{    
    #Get title of active window
    activ_win_title=`xprop -id $activ_win_id | grep "WM_CLASS(STRING)"`   # I used WM_NAME(STRING) before, WM_CLASS more accurate.



    # Check if user want to detect Video fullscreen on Firefox, modify variable firefox_flash_detection if you dont want Firefox detection
    if [ $firefox_flash_detection == 1 ];then
        if [[ "$activ_win_title" = *unknown* || "$activ_win_title" = *plugin-container* ]];then
        # Check if plugin-container process is running
            flash_process=`pgrep -l plugin-containe | grep -wc plugin-containe`
            #(why was I using this line avobe? delete if pgrep -lc works ok)
            #flash_process=`pgrep -lc plugin-containe`
            if [[ $flash_process -ge 1 ]];then
                return 1
            fi
        fi
    fi

    
    # Check if user want to detect Video fullscreen on Chromium, modify variable chromium_flash_detection if you dont want Chromium detection
    if [ $chromium_flash_detection == 1 ];then
        if [[ "$activ_win_title" = *exe* ]];then   
        # Check if Chromium Flash process is running
            if [[ `pgrep -lfc "chromium-browser --type=plugin --plugin-path=/usr/lib/adobe-flashplugin"` -ge 1 || `pgrep -lfc "chromium-browser --type=plugin --plugin-path=/usr/lib/flashplugin-installer"` -ge 1 ]];then
                return 1
            fi
        fi
    fi

    #html5 (Firefox or Chromium full-screen)
    if [ $html5_detection == 1 ];then
        if [[ "$activ_win_title" = *chromium-browser* || "$activ_win_title" = *Firefox* ]];then   
            #check if firefox or chromium is running.
            if [[ `pgrep -l firefox | grep -wc firefox` -ge 1 || `pgrep -l chromium-browse | grep -wc chromium-browse` -ge 1 ]]; then
                return 1
            fi
        fi
    fi

    
    #check if user want to detect mplayer fullscreen, modify variable mplayer_detection
    if [ $mplayer_detection == 1 ];then  
        if [[ "$activ_win_title" = *mplayer* || "$activ_win_title" = *MPlayer* ]];then
            #check if mplayer is running.
            #mplayer_process=`pgrep -l mplayer | grep -wc mplayer`
            mplayer_process=`pgrep -lc mplayer`
            if [ $mplayer_process -ge 1 ]; then
                return 1
            fi
        fi
    fi
    
    
    # Check if user want to detect vlc fullscreen, modify variable vlc_detection
    if [ $vlc_detection == 1 ];then  
        if [[ "$activ_win_title" = *vlc* ]];then
            #check if vlc is running.
            #vlc_process=`pgrep -l vlc | grep -wc vlc`
            vlc_process=`pgrep -lc vlc`
            if [ $vlc_process -ge 1 ]; then
                return 1
            fi
        fi
    fi    
    

return 0
}





#----------------------------------
#
#   MAIN LOOP
#
#----------------------------------


while true; do
    MocpInfo
    VolControl
    RemindInfo
    BatInfo
    LightsOn

    # итоговая панель
    STR="^p(4)$MOCP_STR^pa(1000)$REM_STR^pa(1760)$BAT_STR^pa(1820)$VOL_STR"
    
    echo $STR                   # output goes to the title window
    #dmesg | tail -n 10         # output goes to the slave window
    sleep 0.2
    FIRST_TURN=0
done | dzen2 -ta l -u -x 0 -y 1055 -h 25 -w 1878 -bg $COLOR_BACK -fn $FONT -e "sigusr1=lower,hide;sigusr2=raise,unhide;button3=exit:13" &

exit
